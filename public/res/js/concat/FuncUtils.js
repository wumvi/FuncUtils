'use strict';
/**
 * Перешивает массив
 * @return {Array}
 */
Array.prototype.shuffle = function () {
    var length = this.length, j, temp;
    if (length === 0) {
        return this;
    }
    while (length) {
        length -= 1;
        j = Math.floor(Math.random() * (length + 1));
        temp = this[length];
        this[length] = this[j];
        this[j] = temp;
    }
    return this;
};
/**
 * Получаем массив без дубликатов
 * @return {Array}
 */
Array.prototype.getUnique = function () {
    var uniq = [];
    var count = this.length;
    for (var num = 0; num < count; num += 1) {
        if (uniq.indexOf(this[num]) !== -1) {
            continue;
        }
        uniq.push(this[num]);
    }
    return uniq;
};
/*jshint ignore:start */
window['Array'] = Array;
Array.prototype['getUnique'] = Array.prototype.getUnique;
Array.prototype['shuffle'] = Array.prototype.shuffle;
/* global CanvasEngine */
/**
 *
 * @constructor
 */
function AudioPlayer() {
    /**
   *
   * @type {Object}
   * @private
   */
    this.audioCtrlList_ = {};
}
/**
 *
 * @param {string} name
 * @param {string} audioUrl
 * @param {boolean} isLoadWithoutCache
 */
AudioPlayer.prototype.addSound = function (name, audioUrl, isLoadWithoutCache) {
    var audio = document.createElement('audio');
    //var audio = new Audio();
    isLoadWithoutCache = isLoadWithoutCache !== undefined ? isLoadWithoutCache : false;
    if (!isLoadWithoutCache && (CanvasEngine.isAndroidBrowser() && !CanvasEngine.isNativeAndroidBrowser() || CanvasEngine.isIOs())) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', audioUrl, true);
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var data = xhr.response;
            audio.src = URL.createObjectURL(data);
        };
        xhr.send();
    } else {
        audio.src = audioUrl;
        audio.type = 'audio/mpeg';
        audio.preload = 'auto';
    }
    this.audioCtrlList_[name] = audio;
};
/**
 *
 * @param {string} name
 */
AudioPlayer.prototype.play = function (name) {
    this.audioCtrlList_[name].play();
};
/*jshint ignore:start */
window['AudioPlayer'] = AudioPlayer;
AudioPlayer.prototype['play'] = AudioPlayer.prototype.play;
AudioPlayer.prototype['addSound'] = AudioPlayer.prototype.addSound;
/* global */
/**
 *
 * @param {string} name Имя для подключения
 * @param {string=} opt_subcategory = '' subcategory
 * @constructor
 * @implements {EventDomDispatcherInterface}
 */
function EventDomDispatcher(name, opt_subcategory) {
    if (typeof name !== 'string') {
        throw new Error('The param "name" must be string');
    }
    // Заменяем все не символы, на тире, чтобы не было проблем с селектором у jQuery
    // Если, к примеру, придёт точка, то он начнётся искать класс, а нам это не надо
    var nameClear = this.escape_(name);
    if (opt_subcategory !== undefined && typeof opt_subcategory !== 'string') {
        throw new Error('The param "subcategory" must be string');
    }
    this.subcategory_ = opt_subcategory ? '-' + this.escape_(opt_subcategory) : '';
    /**
   * Название Dom элемента
   * @type {string}
   * @private
   */
    this.fullname_ = this.getDomId(nameClear);
    this.$eventObj_ = jQuery('#' + this.fullname_);
    if (this.$eventObj_.length === 0) {
        var elem = document.createElement('div');
        this.$eventObj_ = jQuery(elem);
        this.$eventObj_.attr('id', this.fullname_);
        jQuery('body:first').append(elem);
    }
}
/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.emit = function (msgName, data) {
    data = data !== undefined ? data : {};
    this.$eventObj_.trigger(msgName + this.subcategory_, data);
};
/**
 *
 * @param {!string} name
 * @return {string}
 * @private
 */
EventDomDispatcher.prototype.escape_ = function (name) {
    return name.replace(/\W/g, '-');
};
/**
 * Слушает сообщения
 * @param {string|number} msgName Название сообщения
 * @param {!string|Function} subcategory Подкатегория
 * @param {Function=} opt_callback Callback-функция, которая вызывается когда наступает событие
 */
EventDomDispatcher.prototype.addListener = function (msgName, subcategory, opt_callback) {
    var callback = opt_callback;
    if (typeof subcategory === 'function') {
        callback = subcategory;
        subcategory = '';
    } else {
        subcategory = '-' + this.escape_(subcategory ? subcategory : '');
    }
    this.$eventObj_.on(msgName + subcategory, function (event, data) {
        callback(event, data);
    });
};
/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.getDomId = function (name) {
    return 'event-' + name;
};
/* jshint ignore:start */
window['EventDomDispatcher'] = EventDomDispatcher;
EventDomDispatcher.prototype['emit'] = EventDomDispatcher.prototype.emit;
EventDomDispatcher.prototype['addListener'] = EventDomDispatcher.prototype.addListener;
EventDomDispatcher.prototype['getDomId'] = EventDomDispatcher.prototype.getDomId;
/**
 * @interface
 */
function EventDomDispatcherInterface() {
}
/**
 * Отправляет сообщение
 *
 * @param {string} msgName Название сообщения
 * @param {*} data Данные
 */
EventDomDispatcherInterface.prototype.emit = function (msgName, data) {
};
/**
 * Слушает сообщения
 * @param {string|number} msgName Название сообщения
 * @param {!string|Function} subcategory Подкатегория
 * @param {Function=} opt_callback Callback-функция, которая вызывается когда наступает событие
 */
EventDomDispatcherInterface.prototype.addListener = function (msgName, subcategory, opt_callback) {
};
/**
 * @param {string} name Название сообытия
 * @return {string} Id DOM объекта
 */
EventDomDispatcherInterface.prototype.getDomId = function (name) {
};
/**
 * @param {Object} event
 * @param {Object} data
 * @constructor
 */
function EventDomDispatcherAddListener(event, data) {
}
/**
 *
 * @constructor
 */
function FUtils() {
}
/**
 *
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
FUtils.getRandomRange = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};
/*jshint ignore:start */
window['FUtils'] = FUtils;
FUtils['getRandomRange'] = FUtils.getRandomRange;    /*jshint ignore:end */