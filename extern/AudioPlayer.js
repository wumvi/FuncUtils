'use strict';

/* global CanvasEngine */



/**
 *
 * @constructor
 */
function AudioPlayer() {

}

/**
 *
 * @param {string} name
 * @param {string} audioUrl
 * @param {boolean} isLoadWithoutCache
 */
AudioPlayer.prototype.addSound = function(name, audioUrl, isLoadWithoutCache) {
};


/**
 *
 * @param {string} name
 */
AudioPlayer.prototype.play = function(name) {
};

