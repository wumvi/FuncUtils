'use strict';
/* global */

/** @typedef {function(Object, Object):void} EventDomDispatcherAddListener */


var EventDomDispatcherAddListener;



/**
 *
 * @param {string} name Имя для подключения
 * @param {string=} opt_subcategory = '' opt_subcategory
 * @constructor
 * @implements {EventDomDispatcherInterface}
 */
function EventDomDispatcher(name, opt_subcategory) {

}


/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.emit = function(msgName, data) {
};


/**
 * Слушает сообщения
 * @param {string|number} msgName Название сообщения
 * @param {!string|Function} subcategory Подкатегория
 * @param {Function=} opt_callback Callback-функция, которая вызывается когда наступает событие
 */
EventDomDispatcher.prototype.addListener = function(msgName, subcategory, opt_callback) {
};


/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.getDomId = function(name) {

};
