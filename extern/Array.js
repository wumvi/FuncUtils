'use strict';

/**
 * Перешивает массив
 * @returns {Array}
 */
Array.prototype.shuffle = function () {
};

/**
 * Получаем массив без дубликатов
 * @returns {Array}
 */
Array.prototype.getUnique = function () {
};