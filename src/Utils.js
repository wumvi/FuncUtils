'use strict';



/**
 *
 * @constructor
 */
function FUtils() {
}


/**
 *
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
FUtils.getRandomRange = function(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};


/**
 *
 * @return {boolean}
 */
FUtils.isAndroidBrowser = function() {
  var navU = navigator.userAgent;
  return navU.indexOf('Android') > -1 && navU.indexOf('Mozilla/5.0') > -1 && navU.indexOf('AppleWebKit') > -1;
};


/**
 *
 * @return {boolean}
 */
FUtils.isNativeAndroidBrowser = function() {
  var navU = navigator.userAgent;
  // Android Mobile
  var isAndroidMobile = FUtils.isAndroidBrowser();

  // Apple webkit
  var regExAppleWebKit = new RegExp(/AppleWebKit\/([\d.]+)/);
  var resultAppleWebKitRegEx = regExAppleWebKit.exec(navU);
  var appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat(regExAppleWebKit.exec(navU)[1]));

  // Chrome
  var regExChrome = new RegExp(/Chrome\/([\d.]+)/);
  var resultChromeRegEx = regExChrome.exec(navU);
  var chromeVersion = resultChromeRegEx === null ? null : parseFloat(regExChrome.exec(navU)[1]);

  // Native Android Browser
  return isAndroidMobile &&
    (appleWebKitVersion !== null && appleWebKitVersion < 537) ||
    (chromeVersion !== null && chromeVersion < 37);
};


/**
 *
 * @return {Array|{index: number, input: string}}
 */
FUtils.isIOs = function() {
  return navigator.userAgent.match(/iPhone|iPad|iPod/i);
};


/*jshint ignore:start */
window['FUtils'] = FUtils;
FUtils['getRandomRange'] = FUtils.getRandomRange;
FUtils['isIOs'] = FUtils.isIOs;
FUtils['isNativeAndroidBrowser'] = FUtils.isNativeAndroidBrowser;
FUtils['isAndroidBrowser'] = FUtils.isAndroidBrowser;
/*jshint ignore:end */
