'use strict';



/**
 *
 * @constructor
 */
function AudioPlayer() {

  /**
   *
   * @type {Object}
   * @private
   */
  this.audioCtrlList_ = {};
}


/**
 *
 * @param {string} name
 * @param {string} audioUrl
 * @param {boolean} isLoadWithoutCache
 */
AudioPlayer.prototype.addSound = function(name, audioUrl, isLoadWithoutCache) {
  var audio = /** @type {HTMLMediaElement} */ (document.createElement('audio'));
  //var audio = new Audio();

  isLoadWithoutCache = isLoadWithoutCache !== undefined ? isLoadWithoutCache : false;

  if (!isLoadWithoutCache && (FUtils.isAndroidBrowser() && !FUtils.isNativeAndroidBrowser() ||
    FUtils.isIOs())) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', audioUrl, true);
    xhr.responseType = 'blob';

    xhr.onload = function() {
      var data = /** @type {!Blob} */ (xhr.response);
      audio.src = URL.createObjectURL(data);
    };
    xhr.send();
  } else {
    audio.src = audioUrl;
    audio.type = 'audio/mpeg';
    audio.preload = 'auto';
  }

  this.audioCtrlList_[name] = audio;
};


/**
 *
 * @param {string} name
 */
AudioPlayer.prototype.play = function(name) {
  this.audioCtrlList_[name].play();
};


/*jshint ignore:start */
window['AudioPlayer'] = AudioPlayer;
AudioPlayer.prototype['play'] = AudioPlayer.prototype.play;
AudioPlayer.prototype['addSound'] = AudioPlayer.prototype.addSound;
/*jshint ignore:end */
