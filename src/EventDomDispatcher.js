'use strict';
/* global */



/**
 *
 * @param {string} name Имя для подключения
 * @param {string=} opt_subcategory = '' subcategory
 * @constructor
 * @implements {EventDomDispatcherInterface}
 */
function EventDomDispatcher(name, opt_subcategory) {
  if (typeof name !== 'string') {
    throw new Error('The param "name" must be string');
  }

  // Заменяем все не символы, на тире, чтобы не было проблем с селектором у jQuery
  // Если, к примеру, придёт точка, то он начнётся искать класс, а нам это не надо
  var nameClear = this.escape_(name);

  if (opt_subcategory !== undefined && typeof opt_subcategory !== 'string') {
    throw new Error('The param "subcategory" must be string');
  }

  this.subcategory_ = opt_subcategory ? '-' + this.escape_(opt_subcategory) : '';

  /**
   * Название Dom элемента
   * @type {string}
   * @private
   */
  this.fullname_ = this.getDomId(nameClear);
  this.$eventObj_ = jQuery('#' + this.fullname_);
  if (this.$eventObj_.length === 0) {
    var elem = document.createElement('div');
    this.$eventObj_ = jQuery(elem);
    this.$eventObj_.attr('id', this.fullname_);
    jQuery('body:first').append(elem);
  }
}


/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.emit = function(msgName, data) {
  data = data !== undefined ? data : {};
  this.$eventObj_.trigger(msgName + this.subcategory_, data);
};


/**
 *
 * @param {!string} name
 * @return {string}
 * @private
 */
EventDomDispatcher.prototype.escape_ = function(name) {
  return name.replace(/\W/g, '-');
};


/**
 * Слушает сообщения
 * @param {string|number} msgName Название сообщения
 * @param {!string|Function} subcategory Подкатегория
 * @param {Function=} opt_callback Callback-функция, которая вызывается когда наступает событие
 */
EventDomDispatcher.prototype.addListener = function(msgName, subcategory, opt_callback) {
  var callback = opt_callback;
  if (typeof subcategory === 'function') {
    callback = /** @type {Function} */ (subcategory);
    subcategory = '';
  } else {
    subcategory = '-' + this.escape_(subcategory ? subcategory : '');
  }

  this.$eventObj_.on(msgName + subcategory, function(event, data) {
    callback(event, data);
  });
};


/**
 * @inheritDoc
 */
EventDomDispatcher.prototype.getDomId = function(name) {
  return 'event-' + name;
};

/* jshint ignore:start */
window['EventDomDispatcher'] = EventDomDispatcher;
EventDomDispatcher.prototype['emit'] = EventDomDispatcher.prototype.emit;
EventDomDispatcher.prototype['addListener'] = EventDomDispatcher.prototype.addListener;
EventDomDispatcher.prototype['getDomId'] = EventDomDispatcher.prototype.getDomId;
/* jshint ignore:end */
