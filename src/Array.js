'use strict';


/**
 * Перешивает массив
 * @return {Array}
 */
Array.prototype.shuffle = function() {
  var length = this.length, j, temp;
  if (length === 0) {
    return this;
  }

  while (length) {
    length -= 1;
    j = Math.floor(Math.random() * (length + 1));
    temp = this[length];
    this[length] = this[j];
    this[j] = temp;
  }
  return this;
};


/**
 * Получаем массив без дубликатов
 * @return {Array}
 */
Array.prototype.getUnique = function() {
  var uniq = [];
  var count = this.length;
  for (var num = 0; num < count; num += 1) {
    if (uniq.indexOf(this[num]) !== -1) {
      continue;
    }

    uniq.push(this[num]);
  }

  return uniq;
};


/*jshint ignore:start */
window['Array'] = Array;
Array.prototype['getUnique'] = Array.prototype.getUnique;
Array.prototype['shuffle'] = Array.prototype.shuffle;
/*jshint ignore:end */
