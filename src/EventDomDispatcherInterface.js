'use strict';



/**
 * @interface
 */
function EventDomDispatcherInterface() {

}


/**
 * Отправляет сообщение
 *
 * @param {string} msgName Название сообщения
 * @param {*} data Данные
 */
EventDomDispatcherInterface.prototype.emit = function(msgName, data) {
};


/**
 * Слушает сообщения
 * @param {string|number} msgName Название сообщения
 * @param {!string|Function} subcategory Подкатегория
 * @param {Function=} opt_callback Callback-функция, которая вызывается когда наступает событие
 */
EventDomDispatcherInterface.prototype.addListener = function(msgName, subcategory, opt_callback) {
};


/**
 * @param {string} name Название сообытия
 * @return {string} Id DOM объекта
 */
EventDomDispatcherInterface.prototype.getDomId = function(name) {
};
