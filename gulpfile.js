'use strict';
/* global require */

var gulp = require('gulp');
var jsConcatDir = './public/res/js/concat/';

var concat = require('gulp-concat-util');
var yaml = require('js-yaml');
var fs = require('fs');
var removeUseStrict = require('gulp-remove-use-strict');

gulp.task('concat-js', function() {
  var buildProperties = yaml.safeLoad(fs.readFileSync('build.yaml', 'utf8'));
  return gulp.src(buildProperties.list)

      .pipe(concat(buildProperties.name))
      .pipe(removeUseStrict())
      .pipe(gulp.dest(jsConcatDir));
});
