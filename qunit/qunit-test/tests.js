
EventDomDispatcher.prototype.getFullName = function(){
    return this.fullname_;
};

QUnit.config.testTimeout  = 500;

/**
 * Тестируем создание объекта
 */
QUnit.test("Wrong param", function (assert) {
	assert.throws(function(){
			var eventDomDispatcher = new EventDomDispatcher();
		}, 
		"Check 1 wrong 'name' param"
	);
	
	assert.throws(function(){
			var eventDomDispatcher = new EventDomDispatcher(true);
		}, 
		"Check 2 wrong 'name' param"
	);
	
	assert.throws(function(){
			var eventDomDispatcher = new EventDomDispatcher('obj-id', true);
		}, 
		"Check 3 wrong 'subcategory' param"
	);
	
	jQuery('#obj-id').remove();
});

/**
 * Тестируем создание объекта
 */
QUnit.test("Create object", function (assert) {
    var elementId = 'event-obj-id';
    var eventDomDispatcher = new EventDomDispatcher('obj-id');

    // Правильно ли генерится название объекта
    assert.ok(eventDomDispatcher.getFullName('obj-id') === elementId, "getFullName is done");

    // Создаётся ли объект DOM
    assert.ok(document.getElementById(elementId) !== null, "Dom object is created");

    jQuery('#' + elementId).remove();
});

/**
 * Тестируем отправки сообщений
 */
QUnit.test("Emit1", function (assert) {
    var eventName1 = 'event-name1';
    var eventName2 = 'event-name2';
    var eventName3 = 'event-name3';
    var elementId = 'obj-id';

    assert.expect(5);
    var done1 = assert.async();
    var done2 = assert.async();
    var done3 = assert.async();

    var eddForTest = new EventDomDispatcher(elementId);
    var eventDomDispatcher = new EventDomDispatcher(elementId);

    // Отправка отправка сообщения без теа
    eddForTest.addListener(eventName1, function(){
        assert.ok(true, 'Message get');
        done1();
    });
    eventDomDispatcher.emit(eventName1);

    // Отправка обычного объекта
    var objValue = {data: 1};
    eddForTest.addListener(eventName2, function(event, data){
        assert.ok(typeof data === 'object', 'Typeof data is object');
        assert.deepEqual(data, objValue, 'Data is the same, that was sended');
        done2();
    });
    eventDomDispatcher.emit(eventName2, objValue);

    function EventMessage(data1, data2){
        this.data1 = data1;
        this.data2 = data2;
    }

    // Отправка объекта класса
    var classObj = new EventMessage(1, 2);
    eddForTest.addListener(eventName3, function(event, data){
        assert.ok(data instanceof EventMessage, 'ClassObject intrace of EventMessage');
        assert.propEqual(data, classObj, 'ClassObject is the same, that was sended');
        done3();
    });
    eventDomDispatcher.emit(eventName3, classObj);

    jQuery('#obj-id').remove();
});

/**
 * Тестируем отправки сообщений
 */
QUnit.test("Emit2", function (assert) {
	assert.expect(2);
    var done1 = assert.async();
	var done2 = assert.async();
	
	var elementId = 'obj-id';
	
	var eventName = 'event-name';
	var subCategory1 = 'subcategory-1';
	var subCategory2 = 'subcategory-2';
		
	var eventDomDispatcher1 = new EventDomDispatcher(elementId, subCategory1);
	var eventDomDispatcher2 = new EventDomDispatcher(elementId, subCategory2);
	
	// Отправка отправка сообщения без теа
    eventDomDispatcher1.addListener(eventName, subCategory1, function(){
        assert.ok(true, 'Message get1');
        done1();
    });

	// Отправка отправка сообщения без теа
    eventDomDispatcher2.addListener(eventName, subCategory2, function(){
        assert.ok(true, 'Message get2');
        done2();
    });
	
	eventDomDispatcher1.emit(eventName);
    eventDomDispatcher2.emit(eventName);
	
	jQuery('#obj-id').remove();
});